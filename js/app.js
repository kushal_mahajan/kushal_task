angular.module('myTask', ['ui.router', 'fullPage.js'])
.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'templates/main.htm',
            controller: "mainCtrl"
        })
         
})
.controller('mainCtrl', ['$scope', '$timeout', '$rootScope', '$http', '$window', '$state', function($scope, $timeout, $rootScope, $http, $window, $state){
	$scope.hideLoader = false;
	$scope.menu = false;

	$http.get('json/site.json').then(function(response){
		
		$timeout(function(){
			$scope.$apply(function(){
				$rootScope.$broadcast('imagesLoaded', { images :response.data[0].images});		
			});
		}, 2000);
		
	});

	$scope.$on('imagesLoaded', function(event, args){
		$scope.hideLoader = true;
		$scope.images = args.images;
	});

	$scope.showMenu = function(){
		$scope.menu = true;
		$('.menuWrap').removeClass('out').addClass('in');
		var myEl = angular.element( document.querySelector( '#logo1' ) );
		$('body').addClass('overflowHidden');
		myEl.addClass('zIndex3');
	}

	$scope.selectMenu = function(){
		
	}
	function addCarousel(){
		var myElm = angular.element(document.querySelector('.main'));
		if($window.innerWidth < 768){
	    	
	    	myElm.addClass('owl-carousel');
				$(".owl-carousel").owlCarousel({
					autoPlay : true,
				    dragBeforeAnimFinish : true,
				    mouseDrag : true,
				    touchDrag : true,
				    pagination : true,
				    paginationNumbers: false,
				    lazyLoad : false,
				    lazyFollow : true,
				    lazyEffect : "fade",
				    singleItem : true
				 });

				$scope.$apply();
				
		}
		else{
			var cnt1 = $(".owl-wrapper").contents();
			$(".owl-wrapper").replaceWith(cnt1);
			var cnt2 = $(".owl-wrapper-outer").contents();
			$(".owl-wrapper-outer").replaceWith(cnt2);
			// $('.owl-carousel').trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
			// $('.owl-carousel').find('.owl-stage-outer').children().unwrap();
			myElm.removeClass('owl-carousel');
			myElm.removeClass('owl-theme');
			$scope.$apply();
			// $state.go('home', {}, {reload: true})

		}	
	}
	addCarousel();	
	angular.element($window).bind('resize', function () {
	    // console.log($window.innerWidth);
	   	addCarousel();
	   
	});


	$scope.closeMenu = function(){
		$('.menuWrap').removeClass('in').addClass('out');
		var myEl = angular.element( document.querySelector( '.menuIcon' ) );
		myEl.addClass('noTransform');
		var myEl2 = angular.element( document.querySelector( '.searchfilter' ) );
		myEl2.addClass('noTransform2');
		$('body').removeClass('overflowHidden');
	}

}])
.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
             if (this.pageYOffset >= 600) {
                 var myElem = angular.element(document.querySelectorAll('.menuIcon span'));
                 myElem.addClass('blackMenuIcon');
               	 		
             } else {
                var myElem = angular.element(document.querySelectorAll('.menuIcon span'));
                 myElem.removeClass('blackMenuIcon');
                 console.log('Header is in view.');
             }
            scope.$apply();
        });
    };
})
.directive('scrollTo', function($window){
	return {
	    restrict: 'A',
	    link: function(scope, $elm, attrs) {
	      var idToScroll = attrs.href;
	      $elm.on('click', function() {
	        var $target;
	        if (idToScroll) {
	          $target = $(idToScroll);
	        } else {
	          $target = $elm;
	        }
	        $("body,html").animate({scrollTop: $target.offset().top }, "slow");
	      });
	    }
	}
})
// .directive('resize', ['$window', function ($window) {

//      return {
//         link: link
//      };

//      function link(scope, element, attrs){

//        scope.width = $window.innerWidth;

//        angular.element($window).bind('resize', function(){

//          scope.width = $window.innerWidth;

//          // manuall $digest required as resize event
//          // is outside of angular
//          scope.$digest();
//        });

//      }

//  }]);
// .directive('onscroll', function($window, $document, $timeout){
// 	 return function(scope, element, attrs) {
//         angular.element($window).bind("scroll", function() {
//         	// var windowHeight = $('window').height();
//         	var windowHeight = $window.innerHeight;
//      
//         	 	$('body, html').animate({scrollTop: windowHeight}, "slow");
//        
//             scope.$apply();
//         });
//     };
// })


