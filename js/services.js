angular.module('myTask')
.factory('AppServices', function($http){
	var getImagesJson = $http({
		url: "json/site.json"
	}).then(function(response){
		return response.data;
	})

	return {
		getImages: getImagesJson
	}
})